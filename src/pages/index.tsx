import ContentPage from "../components/ContentPage";
import "../styles/global.css";

export default ContentPage.bind(this, {
	title: "Decrypted CS4401",
	dirPath: "cs4401/",
	msg: "Decrypted Lecture Notes CS4401 :D, no password"
});
